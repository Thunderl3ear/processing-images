# Assignment 5 -  OpenCV Image Processing


## Table of Contents

- [Requirements](#requirements)
- [Setup](#setup)
- [Program Description](#program-description)
- [Implementation details](#implementation-details)
- [Members' tasks](#members-tasks)
- [Maintainers](#maintainers)
- [Contributers](#contributers)

## Requirements

Requires `gcc`, `cmake`, `curl`, `nlohmann_json`, `restclient-cpp` and `OpenCV`.

## Setup
    $ run 'auto_launch'

OR;

    $ mkdir build
    $ cd build
    $ cmake ../
    $ make
    $ ./main

## Program Description
This program downloads images from the interwebs and does image-manipulation on them for (hopefully) funny result's.

The images are downloaded to images/originals and the processed images are found in images/processed.

## Implementation details
This implementation uses curl library functions for downloading images, as both wget and curl system call's were found to
introduce unknown time delay in the program and temporarily restrict access to the images.
Therefore the native curl solution were chosen, as it provides a deterministic and faster runtime

## Members' tasks
This list describes the tasks completed by each member in the group.

**[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear):**

* Project setup
* Image downloading functionallity
* Restclient interfacing
* Image resizing

**[Christian (@varuld)](https://gitlab.com/varuld):**

* Utility/image manipulation
* Launch script

## Maintainers

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

## Contributors and License
Copyright 2022,

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

[Christian (@varuld)](https://gitlab.com/varuld)
