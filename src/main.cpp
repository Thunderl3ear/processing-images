#include <iostream>
#include <chrono>
#include <thread>
#include "functions.hpp"
#include "opencv_utility.hpp"



int main() 
{
    // Image search parameters
    std::string image_topic = "cake";
    std::string search_url = "https://imsea.herokuapp.com/api/1?q=";

    //Setup the image paths
    std::string originals_path = "../images/original";
    std::string manipulated_path ="../images/processed";
    system_str("mkdir -p "+ originals_path);
    system_str("mkdir -p "+ manipulated_path);

    std::cout<<"Downloading a "+image_topic+" image to "+originals_path<<std::endl;

    // Perform image search. Get image address using restclient-cpp.
    // Download the image using curl.
    restIO image_client = restIO(search_url, originals_path);
    std::string fname = image_client.download_image(image_topic);

    std::cout<<"Loading image from disk into OpenCV."<<std::endl;

    // Load image into memory. Wrapper for imread.
    cv::Mat image_data = load_image(originals_path+"/"+fname);
    // Resize
    cv::Mat image_small = resize_image(image_data, 300);
    // Smooth the image using a Gaussian kernel
	image_small = gauss_blur(image_small);
    // Global thresholding
	image_small = thresholding_binary(image_small);
    //Save to disk. Wrapper for imwrite.
    write_image(manipulated_path+"/"+fname, image_small);

    std::cout<<"Processed image saved to "+manipulated_path + "/"+fname<<std::endl;
    
    return 0;
}
