#ifndef funclib_LIB_H
#define funclib_LIB_H

#include <iostream>
#include <filesystem>
#include "restclient-cpp/restclient.h"
#include <nlohmann/json.hpp>
using json = nlohmann::json;

void system_str(std::string str);

class restIO
{
public:
	restIO(std::string server_URL, std::string image_folder);

    std::string download_image(std::string topic);

private:
	std::string URL;
    std::string folder;
    int count_images();
};

#endif