#ifndef cvlib_LIB_H
#define cvlib_LIB_H
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp> // for blur
#include <opencv2/core/types.hpp> //for Size

// constant, "global" parameters for below functions
constexpr int sigma{3};
constexpr int ksize{(sigma*5)|1};
// defined here __once__, as in DRY

// Wrappers for OpenCV functions
cv::Mat load_image(std::string path);
void write_image(std::string path, cv::Mat image);
cv::Mat resize_image(cv::Mat image, int max_size);
cv::Mat gauss_blur(const cv::Mat src_img);
cv::Mat median_blur(const cv::Mat src_img);
cv::Mat thresholding_binary(const cv::Mat src_img);

#endif
