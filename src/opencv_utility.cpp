#include "opencv_utility.hpp"

// Read a color image from disk
cv::Mat load_image(std::string path)
{
	cv::Mat image;
	image = cv::imread(path,cv::IMREAD_COLOR);
	if(image.empty())
	{
		std::cout << "Could not read the image: " << path << std::endl;
	}
	return image;
}

// Write a cv::Mat object to disk.
// Filetype is included in the path. jpg can save 1 or 3 channel 8bit images.
void write_image(std::string path, cv::Mat image)
{
	cv::imwrite(path, image);
	return;
}

// Resize image to a max_size in either dimension of max_size.
// The ratio of the image is preserved.
cv::Mat resize_image(cv::Mat image, int max_size)
{
	cv::Mat resized_image;
	double ratio_x = (double)max_size/(double)image.cols;
	double ratio_y = (double)max_size/(double)image.rows;
	double ratio_smallest;
	if(ratio_x<ratio_y)
	{
		ratio_smallest = ratio_x;
	}
	else
	{
		ratio_smallest = ratio_y;
	}
	cv::resize(image, resized_image, cv::Size(), ratio_smallest, ratio_smallest, cv::INTER_CUBIC);
	return resized_image;
}

// Blurring with a Gaussian kernel
// following fx at (https://docs.opencv.org/4.x/d6/ddf/samples_2cpp_2laplace_8cpp-example.html#a19)
cv::Mat gauss_blur(const cv::Mat src_img)
{
	cv::Mat return_result_img;

	cv::GaussianBlur(src_img, return_result_img, cv::Size(ksize, ksize), sigma, sigma); 

	return return_result_img;
}

// Median filter blurring
cv::Mat median_blur(const cv::Mat src_img)
{
	cv::Mat return_result_img;
	cv::medianBlur(src_img, return_result_img, ksize);
	return return_result_img;
}

// Global one parameter thresholding
// Converts to grayscale first.
// use thresh, max per example at (https://docs.opencv.org/4.x/d1/d17/samples_2cpp_2ffilldemo_8cpp-example.html#a10)
cv::Mat thresholding_binary(const cv::Mat src_img)
{
	double threshold{127};
	double maxvalue{255};
	cv::Mat dst_img;
	cv::cvtColor(src_img, dst_img, cv::COLOR_BGR2GRAY);
	cv::threshold(dst_img, dst_img, threshold, maxvalue, cv::THRESH_BINARY);
	return dst_img;
}