#include "functions.hpp"
#include <curl/curl.h>

// Wrapper for system calls. Uses C++ style strings instead of C-style.
void system_str(std::string str)
{
    system(str.c_str());
}

// Class for managing GET requests to the image server.
restIO::restIO(std::string server_URL, std::string image_folder)
{
	this->URL = server_URL;
	this->folder = image_folder;
}

// Count the number of images in the download folder
int restIO::count_images()
{
    std::filesystem::path image_path {folder.c_str()};
    int count = 0;
    for (auto& p : std::filesystem::directory_iterator(image_path))
    {
        count++;
    }
    return count;   
}

// Wrapper to fwrite
size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
    size_t written = fwrite(ptr, size, nmemb, stream);
    return written;
}

// Borrowed from the internet. Solves a problem where the wget system call would
// not wait for the file to finish writing to disk before returning.
// This solution integrates the curl library directly.
void curl_download(std::string filepath, std::string url)
{
    CURL *curl;
    FILE *fp;
    CURLcode res;
    curl = curl_easy_init();
    if (curl) {
        fp = fopen(filepath.c_str(),"wb");
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
        fclose(fp);
    }
}

// Query the restclient, parse the json and download the image with curl.
// The image is saved as jpg.
std::string restIO::download_image(std::string topic)
{
    RestClient::Response response = RestClient::get(URL+topic);
    if(response.code==200)
    {
        int count = count_images();
        auto json_body = json::parse(response.body);

        std::string fname = std::to_string(count)+"_"+topic+".jpg";
        std::string url = json_body["results"][2*count];

        // Download and wait until file writing is finished.
        curl_download(folder+"/"+fname, url);
        std::cout<<"Image download complete."<<std::endl;
        return fname;      
    }
    else
    {
        std::cout<<"Could not connect to the image server. "<<"ERROR: "+std::to_string(response.code)<<std::endl;
        return "";
    }
}
